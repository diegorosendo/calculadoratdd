package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculadora {

    public String nome;

    public Calculadora() {
    }

    public int soma(int primeiroNumero, int segundoNumero){

        int resultado =primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero) {
        Double resultado =primeiroNumero + segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }


    public int divisao(int dividendo, int divisor) {
        int quociente = dividendo / divisor;
        return quociente;
    }

    public double divisao(double dividendo, double divisor) {
        double quociente = dividendo / divisor;

        BigDecimal bigDecimal = new BigDecimal(quociente).setScale(2, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }

    public int multiplicacao(int primeiroFator, int segundoFator) {
        int produto = primeiroFator * segundoFator;
        return produto;
    }

    public double multiplicacao(double primeiroFator, double segundoFator) {
        double produto = primeiroFator * segundoFator;

        BigDecimal bigDecimal = new BigDecimal(produto).setScale(2, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }
}

