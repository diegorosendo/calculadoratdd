package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
        calculadora.nome = "Vinicius";
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros(){

        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resultado);
    }


    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){

        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteiros(){

        int quociente = calculadora.divisao(10, 5);

        Assertions.assertEquals(2, quociente);

    }


    @Test
    public void testaADivisaoDeDoisNumerosInteirosNegativos(){

        int quociente = calculadora.divisao(-10, 5);

        Assertions.assertEquals(-2, quociente);

    }

    @Test
    public void testaADivisaoDeDoisFlutuantes(){

        double quociente = calculadora.divisao(10.0, 3.0);

        Assertions.assertEquals(3.33, quociente);

    }






    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteiros(){

        int produto = calculadora.multiplicacao(10, 5);

        Assertions.assertEquals(50, produto);

    }


    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteirosNegativos(){

        int produto = calculadora.multiplicacao(-10, 5);

        Assertions.assertEquals(-50, produto);

    }

    @Test
    public void testaAMultiplicacaoDeDoisFlutuantes(){

        double produto = calculadora.multiplicacao(10.5, 3.0);

        Assertions.assertEquals(31.50, produto);

    }











}
